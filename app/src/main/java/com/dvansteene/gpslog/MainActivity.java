package com.dvansteene.gpslog;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private static final int CHECK_TIME_INTERVAL = 1000;
    private static final int CHECK_DIST_INTERVAL = 0;

    private static final int LOCATION_PERMISSION_RETURN_CODE = 1;
    private static final int WRITING_PERMISSION_RETURN_CODE = 2;

    private boolean loggingPosition;
    private String fileName;

    private TextView latDisplay;
    private TextView lonDisplay;

    private Double latValue;
    private Double lonValue;

    //onLocationChange callback
    //onProviderDisabled callback
    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {

            latValue = location.getLatitude();
            lonValue = location.getLongitude();

            latDisplay.setText( String.valueOf(latValue));
            lonDisplay.setText(String.valueOf(lonValue));
        }

        @Override
        public void onProviderDisabled(String provider) {
            if(provider.equals(LocationManager.GPS_PROVIDER))
                showNoGPSWarning();
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get useful UI element
        ToggleButton sessionToggle = (ToggleButton)findViewById(R.id.toggle_session);
        final ImageButton logButton = (ImageButton)findViewById(R.id.log_button);
        latDisplay = (TextView)findViewById(R.id.lat_value);
        lonDisplay = (TextView)findViewById(R.id.lon_value);

        //setting up textview
        latDisplay.setText(R.string.no_data);
        lonDisplay.setText(R.string.no_data);

        //setting up button
        loggingPosition = false;
        sessionToggle.setChecked(loggingPosition);
        logButton.setEnabled(loggingPosition);

        sessionToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                loggingPosition = isChecked;
                logButton.setEnabled(isChecked);

                if(isChecked){
                    //create csv
                    managingWriting();
                }
                else{
                    //close csv
                    fileName = "";
                }
            }
        });

        sessionToggle.setOnTouchListener(new CompoundButton.OnTouchListener(){

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return inCircle(event,v.getWidth()/2,v.getWidth()/2,v.getHeight()/2);
            }
        });

        logButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                managingWriting();
            }
        });

        logButton.setOnTouchListener(new Button.OnTouchListener(){

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return inCircle(event,v.getWidth()/2,v.getWidth()/2,v.getHeight()/2);
            }
        });

        //start GPS service
        managingGPS();

        //setting up file creation
        fileName = "";

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_RETURN_CODE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    managingGPS();
                } else {

                    // permission denied, disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MainActivity.this, R.string.gps_refused, Toast.LENGTH_SHORT).show();
                    latDisplay.setText(R.string.gps_refused_location_display);
                    lonDisplay.setText(R.string.gps_refused_location_display);
                }
                return;
            }
            case WRITING_PERMISSION_RETURN_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    managingWriting();
                } else {

                    // permission denied, disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MainActivity.this, R.string.writing_refused, Toast.LENGTH_SHORT).show();
                }
                //return; //uncomment if more permission check are needed
            }
        }
    }

    private void managingGPS(){

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, CHECK_TIME_INTERVAL,
                    CHECK_DIST_INTERVAL, mLocationListener);
        }else{
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_RETURN_CODE);
        }
    }

    private void managingWriting(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            //get date time
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
            SimpleDateFormat timerFormat = new SimpleDateFormat("HHmmss");
            String dateStr = dateFormat.format(date);
            String timeStr = timerFormat.format(date);

            //do stuff
            if(fileName.equals("")){
                //we want to create the csv file
                String sFileName;
                try{
                    sFileName=dateFormat.format(date)+"_"+timerFormat.format(date)+".csv";
                    File root = new File(getExternalFilesDir(null).toURI());
                    if (!root.exists()) {
                        root.mkdirs();
                    }
                    File logFile = new File(root, sFileName);
                    if(logFile.createNewFile()) {
                        Toast.makeText(this, R.string.file_created, Toast.LENGTH_SHORT).show();
                        FileWriter writer = new FileWriter(logFile);
                        writer.append(getText(R.string.data_header));
                        writer.append("\n");
                        writer.flush();
                        writer.close();
                    }else {
                        Toast.makeText(this, R.string.already_exist_file, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | NullPointerException e) {
                    e.printStackTrace();
                    sFileName = "";
                }
                fileName=sFileName;
            }else{
                //we want to log current pos
                try{
                    File root = new File(getExternalFilesDir(null).toURI());
                    File logFile = new File(root,fileName);
                    String line = constructLine(dateStr,timeStr,latValue,lonValue);
                    FileOutputStream fileOut = new FileOutputStream(logFile,true);
                    PrintStream writer = new PrintStream(fileOut);
                    writer.print(line+"\n");
                    fileOut.close();
                    Toast.makeText(this, R.string.position_saved, Toast.LENGTH_SHORT).show();
                }
                catch(IOException e){
                    e.printStackTrace();
                }

            }
        }else{
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITING_PERMISSION_RETURN_CODE);
        }
    }

    //construct csv line data
    private String constructLine(String date, String time, Double lat, Double lon){

        String separator = ";";
        String NaN = "NaN";
        String result = date+separator+time+separator;

        if(lat!=null){
            result = result+lat+separator;
        }else{
            result = result+NaN+separator;
        }
        if(lon!=null){
            result = result+lon;
        }else{
            result = result+NaN;
        }

        return result;
    }

    //GPS disabled dialog
    private void showNoGPSWarning() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.gps_disabled_warning)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    //making real round button
    public boolean inCircle(MotionEvent e, float radius, float x, float y) {

        float dx = e.getX() - x;
        float dy = e.getY() - y;
        double d = Math.sqrt((dx * dx) + (dy * dy));

        return d > radius;
    }

}